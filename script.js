document.addEventListener("DOMContentLoaded", function () {
  var account = [];
  var current = [];

  document.getElementById("logout").style.display = "none";
  document.getElementById("remove").style.display = "none";
  document.getElementById("content").style.display = "none";
  document.getElementById("login-error").style.display = "none";

  document.getElementById("login-button").onclick = function () {
    var loginUsernameEntry = document.getElementById("login-username").value;
    var loginPasswordEntry = document.getElementById("login-password").value;

    var storedNames = JSON.parse(localStorage.getItem("account"));
    var login = false;

    if (
      loginUsernameEntry.length != 0 &&
      loginPasswordEntry.length != 0 &&
      storedNames != null
    ) {
      for (let i = 0; i <= storedNames.length; i++) {
        if (
          loginUsernameEntry == storedNames[i][1] &&
          loginPasswordEntry == storedNames[i][2]
        ) {
          current = storedNames[i];
          login = true;
          document.getElementById("form").style.display = "none";
          document.getElementById("logout").style.display = "block";
          document.getElementById("remove").style.display = "block";
          display(login, storedNames);
          break;
        } else {
          document.getElementById("login-error").style.display = "block";
          document.getElementById("login-error").innerHTML =
            "Enter valid username / password";
        }
      }
    }
    if (!login) {
      document.getElementById("login-error").style.display = "block";
      document.getElementById("login-error").innerHTML =
        "Enter valid username / password";
    }
  };

  function display(login, storedNames) {
    if (login) {
      document.getElementsByClassName("content")[0].style.display = "block";
      if (current[6] == "Admin") {
        for (let i = 0; i <= storedNames.length - 1; i++) {
          var newRow = document.createElement("tr");
          for (let x = 0; x <= storedNames[0].length - 1; x++) {
            var newCell = document.createElement("td");
            newCell.innerHTML = storedNames[i][x];
            newRow.append(newCell);
          }
          document.getElementById("rows").appendChild(newRow);
        }
      }

      if (current[6] == "Operations") {
        for (let i = 0; i <= storedNames.length - 1; i++) {
          var newRow = document.createElement("tr");
          for (let x = 0; x <= storedNames[0].length - 1; x++) {
            if (
              storedNames[i][6] == "Operations" ||
              storedNames[i][6] == "Sales"
            ) {
              var newCell = document.createElement("td");
              newCell.innerHTML = storedNames[i][x];
              newRow.append(newCell);
            }
          }
          document.getElementById("rows").appendChild(newRow);
        }
      }

      if (current[6] == "Sales") {
        for (let i = 0; i <= storedNames.length - 1; i++) {
          var newRow = document.createElement("tr");
          for (let x = 0; x <= storedNames[0].length - 1; x++) {
            if (storedNames[i][6] == "Sales") {
              var newCell = document.createElement("td");
              newCell.innerHTML = storedNames[i][x];
              newRow.append(newCell);
            }
          }
          document.getElementById("rows").appendChild(newRow);
        }
      }
    }
  }

  document.getElementById("logout").onclick = function () {
    current = [];
    location.reload(true);
  };

  document.getElementById("remove").onclick = function () {
    localStorage.clear();
    current = [];
    location.reload(true);
  };

  document.getElementById("create-button").onclick = function () {
    var createUsernameEntry = document.getElementById("create-username").value;
    var createPasswordEntry = document.getElementById("create-password").value;
    var createEmailEntry = document.getElementById("create-email").value;
    var createFirstnameEntry = document.getElementById("create-firstname").value;
    var createLastnameEntry = document.getElementById("create-lastname").value;
    var createGenderEntry = document.getElementById("create-gender").value;
    var createRoleEntry = document.getElementById("create-role").value;
    var createEmailValid = false;
    var createUsernameValid = false;
    var createPasswordValid = false;
    var createFirstnameValid = false;
    var createLastnameValid = false;
    var createGenderValid = false;
    var createRoleValid = false;
    var createUsernameObject = document.getElementById("create-username");
    var createPasswordObject = document.getElementById("create-password");
    var createEmailObject = document.getElementById("create-email");
    var createFirstnameObject = document.getElementById("create-firstname");
    var createLastnameObject = document.getElementById("create-lastname");
    var createGenderObject = document.getElementById("genderValue");
    var createRoleObject = document.getElementById("roleValue");
    var validate = /^\s*[a-zA-Z0-9,\s]+\s*$/;
    var validateEmail =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!validateEmail.test(createEmailEntry)) {
      createEmailObject.classList.add("error");
      createEmailObject.value = "Enter a valid email";
    } else {
      createEmailValid = true;
    }

    if (
      !validate.test(createUsernameEntry) ||
      createUsernameEntry.length == 0
    ) {
      createUsernameObject.classList.add("error");
      createUsernameObject.value = "No special characters or spaces.";
    } else {
      createUsernameValid = true;
    }

    if (
      !validate.test(createPasswordEntry) ||
      createPasswordEntry.length == 0
    ) {
      createPasswordObject.classList.add("error");
      createPasswordObject.value = "No special characters or spaces.";
    } else {
      createPasswordValid = true;
    }

    if (
      !validate.test(createFirstnameEntry) ||
      createFirstnameEntry.length == 0
    ) {
      createFirstnameObject.classList.add("error");
      createFirstnameObject.value = "No special characters or spaces.";
    } else {
      createFirstnameValid = true;
    }

    if (
      !validate.test(createLastnameEntry) ||
      createLastnameEntry.length == 0
    ) {
      createLastnameObject.classList.add("error");
      createLastnameObject.value = "No special characters or spaces.";
    } else {
      createLastnameValid = true;
    }

    if (createGenderEntry == "") {
      createGenderObject.innerHTML = "Please select gender";
    } else {
      createGenderValid = true;
    }

    if (createRoleEntry == "") {
      createRoleObject.innerHTML = "Please select role.";
    } else {
      createRoleValid = true;
    }

    if (localStorage.getItem("account") !== null) {
      var storedNames1 = JSON.parse(localStorage.getItem("account"));
      for (let index = 0; index < storedNames1.length; index++) {
        if (createEmailEntry == storedNames1[index][0]) {
          createEmailObject.classList.add("error");
          createEmailObject.value = "Email already exists";
          createEmailValid = false;
        }
      }
    }

    createUsernameObject.addEventListener("click", firstfunction);
    createUsernameObject.addEventListener("focus", firstfunction);

    function firstfunction() {
      createUsernameObject.value = "";
      createUsernameObject.classList.remove("error");
    }

    createPasswordObject.addEventListener("click", secondfunction);
    createPasswordObject.addEventListener("focus", secondfunction);

    function secondfunction() {
      createPasswordObject.value = "";
      createPasswordObject.classList.remove("error");
    }

    createEmailObject.onclick = function () {
      createEmailObject.value = "";
      createEmailObject.classList.remove("error");
    };

    createFirstnameObject.addEventListener("click", thirdfunction);
    createFirstnameObject.addEventListener("focus", thirdfunction);

    function thirdfunction() {
      createFirstnameObject.value = "";
      createFirstnameObject.classList.remove("error");
    }

    createLastnameObject.addEventListener("click", fourthfunction);
    createLastnameObject.addEventListener("focus", fourthfunction);

    function fourthfunction() {
      createLastnameObject.value = "";
      createLastnameObject.classList.remove("error");
    }

    document.getElementById("create-gender").onclick = function () {
      createGenderObject.innerHTML = "Select Gender";
    };

    document.getElementById("create-role").onclick = function () {
      createRoleObject.innerHTML = "Select Role";
    };

    if (
      createUsernameValid == true &&
      createPasswordValid == true &&
      createEmailValid == true &&
      createFirstnameValid == true &&
      createLastnameValid == true &&
      createGenderValid == true &&
      createRoleValid == true
    ) {
      account.push([
        createEmailEntry,
        createUsernameEntry,
        createPasswordEntry,
        createFirstnameEntry,
        createLastnameEntry,
        createGenderEntry,
        createRoleEntry,
      ]);
      localStorage.setItem("account", JSON.stringify(account));
      document.getElementsByClassName("register-form")[0].style.display =
        "none";
      document.getElementsByClassName("login-form")[0].style.display = "block";
    }
  };

  document.querySelectorAll(".message a")[0].onclick = function () {
    document.getElementsByClassName("login-form")[0].style.display = "block";
    document.getElementsByClassName("register-form")[0].style.display = "none";
  };
  document.querySelectorAll(".message a")[1].onclick = function () {
    document.getElementsByClassName("login-form")[0].style.display = "none";
    document.getElementsByClassName("register-form")[0].style.display = "block";
  };
});
